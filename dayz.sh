#!/bin/bash

set -eo pipefail

MODS=()

link_mods() {
  local dir_dayz="${1}"
  local dir_workshop="${2}"
  
  # Iterate mods into array
  # Create a symbolic folder link
  echo "Creating a symbolic link between DayZ and Mods"
  
  for filename in $(ls ${dir_workshop}); do
	local modpath="${dir_workshop}/${filename}"
  
	MODS+=($filename)
	
    if ! [[ -L "${dir_dayz}/${filename}" ]]; then
      echo "Creating mod symlink for: ${filename}"
      ln -sr "${modpath}" "${dir_dayz}/${filename}"
    fi
    
  done
}

main() {
	STEAM_ROOT="${STEAM_ROOT}/steamapps"
	STEAM_APP_ID=221100

	echo "${STEAM_ROOT}"

	local dir_dayz="${STEAM_ROOT}/common/DayZ"
	local dir_workshop="${STEAM_ROOT}/workshop/content/${STEAM_APP_ID}"

	link_mods "${dir_dayz}" "${dir_workshop}" || exit 1

	local mods="$(IFS=";"; echo "${MODS[*]}")"

	local cmd=()
	[[ -n "${mods}" ]] && cmd+=("-mod=${mods}")
	
	steam -applaunch "${STEAM_APP_ID}" "${cmd[@]}"
}

main
